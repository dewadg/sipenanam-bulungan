<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AuthController@login')->name('login');
Route::post('/', 'AuthController@doLogin')->name('auth.login');
Route::post('auth/logout', 'AuthController@doLogout')->name('auth.logout');

Route::get('new-ticket', 'TicketController@create')->name('tickets.create');
Route::post('new-ticket', 'TicketController@store')->name('tickets.store');

Route::group(['prefix' => 'panel', 'middleware' => 'auth'], function () {
    Route::get('report', 'ReportController@index')->name('reports.index');
    Route::get('report/new', 'ReportController@create')->name('reports.create');
    Route::post('report/new', 'ReportController@store')->name('reports.store');
    Route::get('report/{id}', 'ReportController@edit')->name('reports.edit');
    Route::delete('report/{id}', 'ReportController@destroy')->name('reports.destroy');
    Route::get('report/{id}/export', 'ReportController@export')->name('reports.export');

    Route::get('ticket', 'TicketController@index')->name('tickets.index');
    Route::get('ticket/{id}', 'TicketController@view')->name('tickets.view');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('user', 'UserController@index')->name('users.index');
        Route::get('user/new', 'UserController@create')->name('users.create');
        Route::post('user/new', 'UserController@store')->name('users.store');
        Route::get('user/{id}', 'UserController@edit')->name('users.edit');
        Route::patch('user/{id}', 'UserController@update')->name('users.update');
        Route::delete('user/{id}', 'UserController@destroy')->name('users.destroy');
    });
});
