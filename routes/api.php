<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function () {
    Route::get('period', 'Api\PeriodController@index')->name('api.periods.index');
    Route::post('report', 'Api\ReportController@store')->name('api.reports.store');
    Route::get('report/{id}', 'Api\ReportController@get')->name('api.reports.get');
    Route::patch('report/{id}', 'Api\ReportController@update')->name('api.reports.update');
});
