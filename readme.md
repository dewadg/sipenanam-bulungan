## SIPENANAM Bulungan

Sistem Pemantauan, Evaluasi, dan Pelaporan Pelaksanaan Penanaman Modal.

## Installation

1. Run `composer install`
2. Set all required configurations in `.env` file (DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD)
3. Run `php artisan migrate`
4. Run `php artisan db:seed`
5. Run `npm install`
6. Run `npm run prod`
7. Run `php artisan serve` (for development)