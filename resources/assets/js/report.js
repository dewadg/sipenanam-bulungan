import Vue from 'vue'
import ReportForm from './components/ReportForm'

new Vue({
    el: '#app',

    components: {
        ReportForm
    }
})