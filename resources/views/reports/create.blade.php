@section('title', 'Tambah Laporan')
@extends('master')
@section('content')
<!-- Main content -->
<div id="app">
  <report-form></report-form>
</div>
@endsection

@section('breadcrumb')
<li><a href="{{ route('reports.index') }}"><i class="fa fa-file"></i> Laporan</a></li>
<li class="active">@yield('title')</li>
@endsection

@push('header_scripts')
@endpush

@push('footer_scripts')
<script src="{{ asset('js/report.js') }}"></script>
@endpush