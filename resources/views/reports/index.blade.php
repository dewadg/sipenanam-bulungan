@section('title', 'Laporan')
@extends('master')
@section('content')
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Daftar Laporan</h3>
  </div>
  <div class="box-body">
    <table id="report" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Periode</th>
          <th>Perusahaan</th>
          <th>Tgl. Pengesahan</th>
          <th>Bidang Usaha</th>
          <th>Tindakan</th>
        </tr>
      </thead>
      <tbody>
        @foreach($reports as $report)
          <tr>
            <td>{{ $report->period->name }}</td>
            <td>{{ $report->company_name }}</td>
            <td>{{ $report->date_of_endorsement }}</td>
            <td>{{ $report->business_field }}</td>
            <td>
              <a href="{{ route('reports.edit', $report->id) }}" class="btn btn-primary btn-xs">Edit</a>
              <form method="POST" action="{{ route('reports.destroy', $report->id) }}" style="display: inline">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger btn-xs">Hapus</button>
              </form>
              <a href="{{ route('reports.export', $report->id) }}" class="btn btn-default btn-xs">Cetak</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <a href="{{ route('reports.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Laporan</a>
  </div>
  <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection

@section('breadcrumb')
<li><a href="{{ route('reports.index') }}"><i class="fa fa-file"></i> Laporan</a></li>
@endsection

@push('header_scripts')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('footer_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
$(function () {
  $('#report').DataTable();
});
</script>
@endpush