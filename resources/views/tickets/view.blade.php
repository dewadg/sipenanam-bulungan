@section('title', 'Keluhan')
@extends('master')
@section('content')
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Keluhan</h3>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="company_name">Perusahaan</label>
          <input type="text" class="form-control" value="{{ $ticket->company_name }}" readonly>
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text" class="form-control" value="{{ $ticket->email }}" readonly>
        </div>
        <div class="form-group">
          <label for="phone">No. Telpon</label>
          <input type="text" class="form-control" value="{{ $ticket->phone }}" readonly>
        </div>
        <div class="form-group">
          <label for="address">Alamat Perusahaan</label>
          <textarea class="form-control" readonly>{{ $ticket->address }}</textarea>
        </div>
        <div class="form-group">
          <label for="message">Keluhan</label>
          <textarea class="form-control" readonly>{{ $ticket->message }}</textarea>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection

@section('breadcrumb')
<li><a href="{{ route('tickets.index') }}"><i class="fa fa-ticket"></i> Keluhan</a></li>
@endsection

@push('header_scripts')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('footer_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
$(function () {
  $('#ticket').DataTable();
});
</script>
@endpush