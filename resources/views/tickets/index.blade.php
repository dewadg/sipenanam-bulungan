@section('title', 'Keluhan')
@extends('master')
@section('content')
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Daftar Keluhan</h3>
  </div>
  <div class="box-body">
    <table id="ticket" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Perusahaan</th>
          <th>Email</th>
          <th>Pesan</th>
          <th>Tindakan</th>
        </tr>
      </thead>
      <tbody>
        @foreach($tickets as $ticket)
          <tr>
            <td>{{ $ticket->company_name }}</td>
            <td>{{ $ticket->email }}</td>
            <td>{{ $ticket->message }}</td>
            <td>
              <a href="{{ route('tickets.view', $ticket->id) }}" class="btn btn-primary btn-xs">Detil</a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection

@section('breadcrumb')
<li><a href="{{ route('tickets.index') }}"><i class="fa fa-ticket"></i> Keluhan</a></li>
@endsection

@push('header_scripts')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('footer_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
$(function () {
  $('#ticket').DataTable();
});
</script>
@endpush