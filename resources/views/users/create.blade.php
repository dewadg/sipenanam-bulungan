@section('title', 'Tambah Pengguna')
@extends('master')
@section('content')
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h4 class="box-title">Lengkapi formulir di bawah</h4>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-6">
          @if(count($errors) > 0)
            @foreach($errors->all() as $error)
              <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ $error }}
              </div>
            @endforeach
          @endif
          <form action="{{ route('users.store') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="role_id" class="col-xs-2 control-label">Level Akses</label>
              <div class="col-xs-10">
                <select name="role_id" class="form-control">
                  @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="col-xs-2 control-label">Nama Pengguna</label>
              <div class="col-xs-10">
                <input name="name" type="text" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="full_name" class="col-xs-2 control-label">Nama Lengkap</label>
              <div class="col-xs-10">
                <input name="full_name" type="text" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-xs-2 control-label">Kata Sandi</label>
              <div class="col-xs-10">
                <input name="password" type="password" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="password_conf" class="col-xs-2 control-label">Konfirmasi Kata Sandi</label>
              <div class="col-xs-10">
                <input name="password_conf" type="password" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-10 col-xs-offset-2">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection

@section('breadcrumb')
<li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> Pengguna</a></li>
<li class="active">Tambah Pengguna</li>
@endsection