@section('title', 'Pengguna')
@extends('master')
@section('content')
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Daftar Pengguna</h3>
  </div>
  <div class="box-body">
    <table id="user" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>Nama Pengguna</th>
          <th>Nama Lengkap</th>
          <th>Level Akses</th>
          <th>Tindakan</th>
        </tr>
      </thead>
      <tbody>
        @foreach($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->full_name }}</td>
            <td>{{ $user->role->name }}</td>
            <td>
              <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary btn-xs">Edit</a>
              <form method="POST" action="{{ route('users.destroy', $user->id) }}" style="display: inline">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger btn-xs">Hapus</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <a href="{{ route('users.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Pengguna</a>
  </div>
  <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
@endsection

@section('breadcrumb')
<li><a href="{{ route('users.index') }}"><i class="fa fa-users"></i> Pengguna</a></li>
@endsection

@push('header_scripts')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('footer_scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
$(function () {
  $('#user').DataTable();
});
</script>
@endpush