<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'period_id',
        'company_name',
        'notarial_deed',
        'date_of_endorsement',
        'npwp',
        'business_field',
        'project_location_address',
        'correspondence_address',
        'capital_investment_license',
        'production_importer_id',
        'customs_facility',
        'fiscal_facility',
        'human_resources_planning',
        'location_license',
        'land_certificate',
        'building_permit',
        'ho',
        'other_technical_permit',
        'working_capital',
        'land_purchase',
        'building_purchase',
        'tools_purchase',
        'others_purchase',
        'self_capital',
        'replanted_profit',
        'loan_capital',
        'number_of_men',
        'number_of_women',
        'number_of_foreigners',
        'description',
        'user_id'
    ];

    public function period()
    {
        return $this->belongsTo(Period::class);
    }

    public function investmentTotal()
    {
        return $this->building_purchase + $this->land_purchase + $this->tools_purchase + $this->others_purchase + $this->working_capital;
    }

    public function sourceOfFundsTotal()
    {
        return $this->self_capital + $this->replanted_profit + $this->loan_capital;
    }
}
