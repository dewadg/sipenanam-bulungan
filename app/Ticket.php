<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'company_name',
        'address',
        'email',
        'phone',
        'message'
    ];
}
