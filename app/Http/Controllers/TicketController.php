<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use DB;

class TicketController extends Controller
{
    public function index()
    {
        $tickets = Ticket::get();

        return view('tickets.index', compact('tickets'));
    }

    public function create()
    {
        return view('tickets.new');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required',
            'email' => 'required',
            'address' => 'required',
            'phone' => 'required|max:15',
            'message' => 'required'
        ], [
            'company_name.required' => 'Nama perusahaan tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'address.required' => 'Alamat perusahaan tidak boleh kosong',
            'phone.required' => 'No telpon tidak boleh kosong',
            'phone.max' => 'No telpon tidak boleh lebih dari 15 karakter',
            'message.required' => 'Pesan tidak boleh kosong'
        ]);

        try {   
            DB::beginTransaction();
            Ticket::create($request->all());
            DB::commit();

            return view('tickets.success');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()
                ->back()
                ->withErrors(collect([$e->getMessage()]));
        }
    }

    public function view($id)
    {
        $ticket = Ticket::find($id);

        if (is_null($ticket)) {
            abort(404);
        }
        
        return view('tickets.view', compact('ticket'));
    }
}
