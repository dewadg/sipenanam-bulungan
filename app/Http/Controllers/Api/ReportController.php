<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;
use Validator;
use DB;

class ReportController extends Controller
{
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'period_id' => 'required',
            'company_name' =>  'required',
            'notarial_deed' => 'required',
            'date_of_endorsement' => 'required|date',
            'npwp' => 'required|max:20',
            'business_field' => 'required',
            'project_location_address' => 'required',
            'correspondence_address' => 'required',
            'capital_investment_license' => 'required',
            'working_capital' => 'required',
            'land_purchase' => 'required',
            'building_purchase' => 'required',
            'tools_purchase' => 'required',
            'others_purchase' => 'required',
            'self_capital' => 'required',
            'replanted_profit' => 'required',
            'loan_capital' => 'required',
            'number_of_men' => 'required',
            'number_of_women' => 'required',
            'number_of_foreigners' => 'required',
            'description' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
                ->json(['result' => 'failed', 'errors' => $validation->errors()], 400);
        }

        try {
            $user_data = $request->all();
            $user_data['user_id'] = 1;

            DB::beginTransaction();
            Report::create($user_data);
            DB::commit();

            return response()
                ->json(['result' => 'success', 'data' => ['redirect' => route('reports.index')]]);
        } catch(\Exception $e) {
            DB::rollback();
            return response()
                ->json(['result' => 'failed', 'errors' => [$e->getMessage()]], 500);
        }
    }

    public function get($id)
    {
        $report = Report::find($id);

        if (is_null($report)) {
            return response()
                ->json(['result' => 'not_found'], 404);
        }

        return response()
            ->json($report);
    }

    public function update(Request $request, $id)
    {
        $report = Report::find($id);

        if (is_null($report)) {
            return response()
                ->json(['result' => 'not_found'], 404);
        }

        try {
            DB::beginTransaction();
            $report->update($request->all());
            DB::commit();

            return response()
                ->json(['result' => 'success', 'data' => ['redirect' => route('reports.index')]]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()
                ->json(['result' => 'failed', 'errors' => [$e->getMessage()]], 500);
        }
    }
}
