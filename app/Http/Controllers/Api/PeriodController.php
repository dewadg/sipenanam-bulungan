<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Period;

class PeriodController extends Controller
{
    public function index()
    {
        $periods = Period::all();

        return response()
            ->json($periods);
    }
}
