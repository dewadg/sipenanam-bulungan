<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ]);
        $user_data = $request->except('_token');

        if (Auth::attempt($user_data)) {
            return redirect()
                ->route('reports.index');
        }

        return redirect()
            ->back()
            ->withErrors(collect(['Nama pengguna/kata sandi salah']));
    }

    public function doLogout()
    {
        Auth::logout();

        return redirect()
            ->route('login');
    }
}
