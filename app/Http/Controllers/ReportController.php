<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Report;
use App\Period;
use Excel;

class ReportController extends Controller
{
    public function index()
    {
        $reports = Auth::user()->reports;

        return view('reports.index', compact('reports'));
    }

    public function create()
    {
        $periods = Period::all();

        return view('reports.create', compact('periods'));
    }

    public function edit($id)
    {
        $report = Report::find($id);

        if (Gate::denies('update-report', $report)) {
            abort(404);
        }

        return view('reports.edit', compact('id'));
    }

    public function destroy($id)
    {
        $report = Report::find($id);

        if (Gate::denies('view-report', $report)) {
            abort(404);
        }

        $report->delete();

        return redirect()
            ->route('reports.index');
    }

    public function export($id)
    {
        $report = Report::find($id);

        if (is_null($report)) {
            abort(404);
        }

        Excel::create('laporan', function ($excel) use ($report) {
            $excel->sheet('Sheet1', function ($sheet) use ($report) {
                $sheet->setWidth('A', 4);
                $sheet->setWidth('B', 20);
                $sheet->setWidth('c', 20);

                $sheet->mergeCells('A1:H1');
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('LAPORAN PELAKSANAAN PENANAMAN MODAL');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A2:H2');
                $sheet->cell('A2', function ($cell) use ($report) {
                    $cell->setValue('PERIODE ' . strtoupper($report->period->name));
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A3:H3');
                $sheet->cell('A3', function ($cell) use ($report) {
                    $cell->setValue('DICETAK PADA ' . date('Y-m-d H:i:s'));
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                // Company-related
                $sheet->mergeCells('A5:H5');
                $sheet->cell('A5', function ($cell) use ($report) {
                    $cell->setValue('KETERANGAN PERUSAHAAN');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A6:C6');
                $sheet->cell('A6', function ($cell) use ($report) {
                    $cell->setValue('NAMA PERUSAHAAN');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D6:H6');
                $sheet->cell('D6', function ($cell) use ($report) {
                    $cell->setValue($report->company_name);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A7:C7');
                $sheet->cell('A7', function ($cell) use ($report) {
                    $cell->setValue('AKTA NOTARIS');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D7:H7');
                $sheet->cell('D7', function ($cell) use ($report) {
                    $cell->setValue($report->notarial_deed);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A8:C8');
                $sheet->cell('A8', function ($cell) use ($report) {
                    $cell->setValue('TANGGAL PENGESAHAN');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D8:H8');
                $sheet->cell('D8', function ($cell) use ($report) {
                    $cell->setValue($report->date_of_endorsement);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A9:C9');
                $sheet->cell('A9', function ($cell) use ($report) {
                    $cell->setValue('NPWP');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D9:H9');
                $sheet->cell('D9', function ($cell) use ($report) {
                    $cell->setValue($report->npwp);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A10:C10');
                $sheet->cell('A10', function ($cell) use ($report) {
                    $cell->setValue('BIDANG USAHA');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D10:H10');
                $sheet->cell('D10', function ($cell) use ($report) {
                    $cell->setValue($report->business_field);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A11:C11');
                $sheet->cell('A11', function ($cell) use ($report) {
                    $cell->setValue('ALAMAT PERUSAHAAN');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D11:H11');
                $sheet->cell('D11', function ($cell) use ($report) {
                    $cell->setValue($report->project_location_address);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A12:C12');
                $sheet->cell('A12', function ($cell) use ($report) {
                    $cell->setValue('ALAMAT KORESPONDENSI');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D12:H12');
                $sheet->cell('D12', function ($cell) use ($report) {
                    $cell->setValue($report->correspondence_address);
                    $cell->setAlignment('center');
                });

                // Licences & Permits
                $sheet->mergeCells('A14:H14');
                $sheet->cell('A14', function ($cell) use ($report) {
                    $cell->setValue('PERIZINAN');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A15:C15');
                $sheet->cell('A15', function ($cell) use ($report) {
                    $cell->setValue('IZIN PRINSIP PENANAMAN MODAL');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D15:H15');
                $sheet->cell('D15', function ($cell) use ($report) {
                    $cell->setValue($report->capital_investment_license);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A16:C16');
                $sheet->cell('A16', function ($cell) use ($report) {
                    $cell->setValue('ANGKA PENGENAL IMPORTIR PRODUSEN');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D16:H16');
                $sheet->cell('D16', function ($cell) use ($report) {
                    $cell->setValue($report->production_importer_id);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A17:C17');
                $sheet->cell('A17', function ($cell) use ($report) {
                    $cell->setValue('FASILITAS BEA MASUK ATAS IMPOR');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D17:H17');
                $sheet->cell('D17', function ($cell) use ($report) {
                    $cell->setValue($report->customs_facility);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A18:C18');
                $sheet->cell('A18', function ($cell) use ($report) {
                    $cell->setValue('FASILITAS FISKAL LAINNYA');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D18:H18');
                $sheet->cell('D18', function ($cell) use ($report) {
                    $cell->setValue($report->fiscal_facility);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A19:C19');
                $sheet->cell('A19', function ($cell) use ($report) {
                    $cell->setValue('RENCANA PENGGUNAAN TENAGA KERJA');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D19:H19');
                $sheet->cell('D19', function ($cell) use ($report) {
                    $cell->setValue($report->human_resources_planning);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A20:C20');
                $sheet->cell('A20', function ($cell) use ($report) {
                    $cell->setValue('IZIN LOKASI');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D20:H20');
                $sheet->cell('D20', function ($cell) use ($report) {
                    $cell->setValue($report->location_license);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A21:C21');
                $sheet->cell('A21', function ($cell) use ($report) {
                    $cell->setValue('SK HAK ATAS TANAH/SERTIFIKAT');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D21:H21');
                $sheet->cell('D21', function ($cell) use ($report) {
                    $cell->setValue($report->land_certificate);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A22:C22');
                $sheet->cell('A22', function ($cell) use ($report) {
                    $cell->setValue('IMB');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D22:H22');
                $sheet->cell('D22', function ($cell) use ($report) {
                    $cell->setValue($report->building_permit);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A23:C23');
                $sheet->cell('A23', function ($cell) use ($report) {
                    $cell->setValue('IZIN UU GANGGUAN/HO');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D23:H23');
                $sheet->cell('D23', function ($cell) use ($report) {
                    $cell->setValue($report->ho);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A24:C24');
                $sheet->cell('A24', function ($cell) use ($report) {
                    $cell->setValue('IZIN TEKNIS LAINNYA');
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('D24:H24');
                $sheet->cell('D24', function ($cell) use ($report) {
                    $cell->setValue($report->other_technical_permit);
                    $cell->setAlignment('center');
                });

                // Investments
                $sheet->mergeCells('A26:H26');
                $sheet->cell('A26', function ($cell) use ($report) {
                    $cell->setValue('REALISASI INVESTASI');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A27:H27');
                $sheet->cell('A27', function ($cell) use ($report) {
                    $cell->setValue('A. INVESTASI');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A28:C28');
                $sheet->cell('A28', function ($cell) use ($report) {
                    $cell->setValue('1. MODAL TETAP');
                });

                $sheet->mergeCells('B29:C29');
                $sheet->cell('B29', function ($cell) use ($report) {
                    $cell->setValue('a. Pembelian & Pematangan Tanah');
                });
                $sheet->mergeCells('D29:H29');
                $sheet->cell('D29', function ($cell) use ($report) {
                    $cell->setValue($report->land_purchase);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('B30:C30');
                $sheet->cell('B30', function ($cell) use ($report) {
                    $cell->setValue('b. Gedung & Bangunan');
                });
                $sheet->mergeCells('D30:H30');
                $sheet->cell('D30', function ($cell) use ($report) {
                    $cell->setValue($report->building_purchase);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('B31:C31');
                $sheet->cell('B31', function ($cell) use ($report) {
                    $cell->setValue('c. Mesin & Peralatan/Suku Cadang');
                });
                $sheet->mergeCells('D31:H31');
                $sheet->cell('D31', function ($cell) use ($report) {
                    $cell->setValue($report->tools_purchase);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('B32:C32');
                $sheet->cell('B32', function ($cell) use ($report) {
                    $cell->setValue('d. Lain-lain');
                });
                $sheet->mergeCells('D32:H32');
                $sheet->cell('D32', function ($cell) use ($report) {
                    $cell->setValue($report->others_purchase);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A33:C33');
                $sheet->cell('A33', function ($cell) use ($report) {
                    $cell->setValue('2. MODAL KERJA');
                });
                $sheet->mergeCells('D33:H33');
                $sheet->cell('D33', function ($cell) use ($report) {
                    $cell->setValue($report->working_capital);
                    $cell->setAlignment('center');
                });

                $sheet->cell('C34', function ($cell) use ($report) {
                    $cell->setValue('Total');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('D34:H34');
                $sheet->cell('D34', function ($cell) use ($report) {
                    $cell->setValue($report->investmentTotal());
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A36:H36');
                $sheet->cell('A36', function ($cell) use ($report) {
                    $cell->setValue('B. SUMBER BIAYA');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A37:C37');
                $sheet->cell('A37', function ($cell) use ($report) {
                    $cell->setValue('1. MODAL SENDIRI');
                });
                $sheet->mergeCells('D37:H37');
                $sheet->cell('D37', function ($cell) use ($report) {
                    $cell->setValue($report->self_capital);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A38:C38');
                $sheet->cell('A38', function ($cell) use ($report) {
                    $cell->setValue('2. PROFIT DITANAM KEMBALI');
                });
                $sheet->mergeCells('D38:H38');
                $sheet->cell('D38', function ($cell) use ($report) {
                    $cell->setValue($report->replanted_profit);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A39:C39');
                $sheet->cell('A39', function ($cell) use ($report) {
                    $cell->setValue('3. MODAL PINJAMAN');
                });
                $sheet->mergeCells('D39:H39');
                $sheet->cell('D39', function ($cell) use ($report) {
                    $cell->setValue($report->loan_capital);
                    $cell->setAlignment('center');
                });
                $sheet->cell('C40', function ($cell) use ($report) {
                    $cell->setValue('Total');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('D40:H40');
                $sheet->cell('D40', function ($cell) use ($report) {
                    $cell->setValue($report->sourceOfFundsTotal());
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A42:H42');
                $sheet->cell('A42', function ($cell) use ($report) {
                    $cell->setValue('TENAGA KERJA');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('A43:C43');
                $sheet->cell('A43', function ($cell) use ($report) {
                    $cell->setValue('INDONESIA');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B44:C44');
                $sheet->cell('B44', function ($cell) use ($report) {
                    $cell->setValue('1. Laki-laki');
                });
                $sheet->mergeCells('D44:H44');
                $sheet->cell('D44', function ($cell) use ($report) {
                    $cell->setValue($report->number_of_men);
                    $cell->setAlignment('center');
                });
                $sheet->mergeCells('B45:C45');
                $sheet->cell('B45', function ($cell) use ($report) {
                    $cell->setValue('2. Perempuan');
                });
                $sheet->mergeCells('D45:H45');
                $sheet->cell('D45', function ($cell) use ($report) {
                    $cell->setValue($report->number_of_women);
                    $cell->setAlignment('center');
                });
                $sheet->mergeCells('B44:C44');
                $sheet->cell('B44', function ($cell) use ($report) {
                    $cell->setValue('1. Laki-laki');
                });
                $sheet->mergeCells('B44:C44');
                $sheet->cell('B44', function ($cell) use ($report) {
                    $cell->setValue('1. Laki-laki');
                });
                $sheet->cell('C46', function ($cell) use ($report) {
                    $cell->setValue('Total');
                    $cell->setAlignment('right');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('D46:H46');
                $sheet->cell('D46', function ($cell) use ($report) {
                    $cell->setValue(($report->number_of_men + $report->number_of_women));
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('A47:C47');
                $sheet->cell('A47', function ($cell) use ($report) {
                    $cell->setValue('ASING');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('D47:H47');
                $sheet->cell('D47', function ($cell) use ($report) {
                    $cell->setValue($report->number_of_foreigners);
                    $cell->setAlignment('center');
                });

                $sheet->mergeCells('A49:H49');
                $sheet->cell('A49', function ($cell) use ($report) {
                    $cell->setValue('PERMASALAHAN');
                    $cell->setAlignment('center');
                    $cell->setFontWeight('bold');
                });
                $sheet->mergeCells('A50:H50');
                $sheet->cell('A50', function ($cell) use ($report) {
                    $cell->setValue($report->description);
                });
            });
        })->download('xlsx');
    }
}
