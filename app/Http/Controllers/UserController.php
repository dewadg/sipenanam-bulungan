<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use DB;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::get();

        return view('users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:users,name',
            'full_name' => 'required',
            'role_id' => 'required',
            'password' => 'required',
            'password_conf' => 'required|same:password'
        ], [
            'name.required' => 'Nama pengguna tidak boleh kosong',
            'name.unique' => 'Nama pengguna sudah terdaftar',
            'full_name.required' => 'Nama lengkap tidak boleh kosong',
            'password.required' => 'Kata sandi tidak boleh kosong',
            'password_conf.required' => 'Konfirmasi kata sandi tidak boleh kosong',
            'password_conf.same' => 'Kata sandi tidak cocok'
        ]);
        $user_data = $request->all();
        $user_data['password'] = bcrypt($user_data['password']);

        try {
            DB::beginTransaction();
            User::create($user_data);
            DB::commit();

            return redirect()
                ->route('users.index');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()
                ->back()
                ->withErrors(collect([$e->getMessage()]));
        }
    }

    public function edit($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            abort(404);
        }

        $roles = Role::get();

        return view('users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            abort(404);
        }

        $this->validate($request, [
            'full_name' => 'required',
            'password' => 'sometimes',
            'password_conf' => 'sometimes|same:password'
        ]);
        $user_data = $request->all();
        $user_data['password'] = ! empty($user_data['password']) ? bcrypt($user_data['password']) : null;

        try {
            DB::beginTransaction();
            $user->update($user_data);
            DB::commit();

            return redirect()
                ->route('users.index');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()
                ->back()
                ->withErrors(collect([$e->getMessage()]));
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            abort(404);
        }

        $user->delete();

        return redirect()
            ->route('users.index');
    }
}
