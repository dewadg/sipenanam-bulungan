<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('notarial_deed');
            $table->date('date_of_endorsement');
            $table->string('npwp');
            $table->text('project_location_address');
            $table->text('correspondence_address');

            $table->string('capital_investment_license');
            $table->string('production_importer_id')->nullable();
            $table->string('customs_facility')->nullable();
            $table->string('fiscal_facility')->nullable();
            $table->string('human_resources_planning')->nullable();
            $table->string('location_license')->nullable();
            $table->string('land_certificate')->nullable();
            $table->string('building_permit')->nullable();
            $table->string('ho')->nullable();
            $table->string('other_technical_permit')->nullable();

            $table->double('land_purchase', 15, 2);
            $table->double('building_purchase', 15, 2);
            $table->double('tools_purchase', 15, 2);
            $table->double('others_purchase', 15, 2);

            $table->double('self_capital', 15, 2);
            $table->double('replanted_profit', 15, 2);
            $table->double('loan_capital', 15, 2);

            $table->integer('number_of_men');
            $table->integer('number_of_women');
            $table->integer('number_of_foreigners');
            $table->integer('period_id')->unsigned();
            $table->foreign('period_id')
                ->references('id')->on('periods');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
