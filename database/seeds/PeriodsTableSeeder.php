<?php

use Illuminate\Database\Seeder;

class PeriodsTableSeeder extends Seeder
{
    private $periods = [
        [
            'id' => 1,
            'name' => 'Triwulan Pertama (Januari-Maret)'
        ],
        [
            'id' => 2,
            'name' => 'Triwulan Kedua (April-Juni)'
        ],
        [
            'id' => 3,
            'name' => 'Triwulan Ketiga (Juli-September)'
        ],
        [
            'id' => 4,
            'name' => 'Triwulan Keempat (Oktober-Desember)'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->periods as $period) {
            App\Period::create($period);
        }
    }
}
