<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'admin',
            'full_name' => 'Administrator',
            'password' => bcrypt('admin2017'),
            'role_id' => 1,
        ]);
    }
}
