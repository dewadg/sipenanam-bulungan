-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: dev_sp2017
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2017_07_26_093526_create_periods_table',1),(3,'2017_07_26_093803_create_reports_table',1),(4,'2017_07_26_141826_create_roles_table',1),(5,'2017_07_26_152317_update_reports_table_add_business_field_column',2),(6,'2017_07_26_185134_update_reports_table_add_working_capital_column',3),(7,'2017_07_27_091131_update_reports_table_remove_user_id_column',4),(8,'2017_07_27_095634_create_tickets_table',4),(9,'2017_07_27_174727_update_reports_table_add_description_column',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periods`
--

DROP TABLE IF EXISTS `periods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periods`
--

LOCK TABLES `periods` WRITE;
/*!40000 ALTER TABLE `periods` DISABLE KEYS */;
INSERT INTO `periods` VALUES (1,'Triwulan Pertama (Januari-Maret)','2017-07-26 07:43:50','2017-07-26 07:43:50'),(2,'Triwulan Kedua (April-Juni)','2017-07-26 07:43:50','2017-07-26 07:43:50'),(3,'Triwulan Ketiga (Juli-September)','2017-07-26 07:43:50','2017-07-26 07:43:50'),(4,'Triwulan Keempat (Oktober-Desember)','2017-07-26 07:43:50','2017-07-26 07:43:50');
/*!40000 ALTER TABLE `periods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notarial_deed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_endorsement` date NOT NULL,
  `npwp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_location_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correspondence_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `capital_investment_license` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `production_importer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customs_facility` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fiscal_facility` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `human_resources_planning` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_license` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_certificate` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building_permit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ho` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_technical_permit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_purchase` double(15,2) NOT NULL,
  `building_purchase` double(15,2) NOT NULL,
  `tools_purchase` double(15,2) NOT NULL,
  `others_purchase` double(15,2) NOT NULL,
  `self_capital` double(15,2) NOT NULL,
  `replanted_profit` double(15,2) NOT NULL,
  `loan_capital` double(15,2) NOT NULL,
  `number_of_men` int(11) NOT NULL,
  `number_of_women` int(11) NOT NULL,
  `number_of_foreigners` int(11) NOT NULL,
  `period_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `business_field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `working_capital` double(15,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reports_period_id_foreign` (`period_id`),
  CONSTRAINT `reports_period_id_foreign` FOREIGN KEY (`period_id`) REFERENCES `periods` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` VALUES (1,'PT Maju Terus','33 33','2017-07-14','33.333.333.3-333.333','Jl. Kaliurang','Jl. Solo','33 44 55','T1','T2','T3','T4','T5','T6','T7','T8',NULL,50.00,50.00,50.00,50.00,250.00,250.00,1000.00,2,1,2,4,'2017-07-26 21:07:18','2017-07-27 10:50:47','Ternak Lele',50.00,'Tes tes tes');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator'),(2,'Operator');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (1,'PT Maju Terus','Jl. Kaliurang','info@majuterus.com','082333444555','Kok gak bisa login ya','2017-07-27 03:19:10','2017-07-27 03:19:10');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','Administrator','$2y$10$CuLcotUuftuePFsI6UbPU.p/EmfkQwybIJ.sgAzySENPizuYbODN.','e4tTbYIoF6y5zjoD7EpzPOj669nFYgFKfcNnrh5FauWPT43loLqGFcX2Bbse','2017-07-26 07:25:49','2017-07-26 07:25:49',1),(3,'denicho','Deni Choz','$2y$10$PJeGHAHSqw/4.oi/h5ozP.k4mVvz/IFou1vrZz4aVDeEP9Ss8KgwK','MIcG7H443DbVaOsSzhIez0Hhvlh0sHlpPMkJ6e6nwLreKWhnj36m7fLOwbzH','2017-07-27 02:31:00','2017-07-27 02:55:19',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-28  3:46:23
